import { Ref, ref } from "vue";

export default class tag {
    key: number = Math.random();
    #times = 2000;
    titleTag: string;
    isMoveItem: Ref<boolean> = ref(false);

    constructor(titleTag: string, cd: Function) {
        this.titleTag = titleTag
        this.move()
        setTimeout(() => {
            cd()
        }, this.#times);
    }

    move() {
        setTimeout(() => {
            this.isMoveItem.value = true
        });
    }
}